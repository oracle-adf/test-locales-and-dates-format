package org.javadev.beans;

import java.util.Date;

import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichChooseDate;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;

public class MyLogic {

    private RichPanelGroupLayout myPGL;
    
    public void setMyPGL(RichPanelGroupLayout myPGL) {
        this.myPGL = myPGL;
    }

    public RichPanelGroupLayout getMyPGL() {
        return myPGL;
    }
    
    // -----------------------------------------------------

    public String getDummy(){
        return (new Date()).toString();
    }


    public void onChangeLocaleClick(ActionEvent actionEvent) {
        
        System.out.println("Click");
        
        Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        System.out.println("CURRENT LOCALE " + currentLocale.toString());
        
        String newLocaleName;
        
        if (currentLocale.getLanguage().startsWith("ru")){
            newLocaleName = "en_GB";
        } else {
            newLocaleName = "ru_RU";
        }
        
        System.out.println("NEW LOCALE " + newLocaleName);
        
        Locale newLocale = new Locale(newLocaleName);  
        FacesContext.getCurrentInstance().getViewRoot().setLocale(newLocale);
        
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(myPGL);
        
    }
}
